import random

def print_command_guide():
    mssg = """
Available commands:
 
    B - Converts a decimal number to binary - Task #1
    D - Converts a binary number to decimal - Task #2
    M - Math practice for multiplications between 0 and 10 - Task #3
    H - Prints this menu.
    Q - Quit, exits this program.
    """
    print(mssg)

def to_binary():
    Number = input("Write a number: ")
    
    try:
        NumberCheck = int(Number)
        binary=bin(NumberCheck)
        print(binary)
        
    except ValueError:
        print("Invalid. Write a number.")
        
def to_decimal():
    BinaryNumber = input("Write a valid binary number: ")
    
    try:
        Number = int(BinaryNumber, 2)
        print(Number)
    
    except ValueError:
        invalid="""
            Yo 
            That's invalid. 
              
            Write something like 1101 or 0b101
            """
        print(invalid)
def multiplication_practise():
    while True:
        Question_Number_1 = random.randint(1, 10)
        Question_Number_2 = random.randint(1, 10)


        answer = Question_Number_1 + Question_Number_2


        tries = 0

        while tries < 3:
            
            User_Input = int(input(f"What is {Question_Number_1} + {Question_Number_2}? "))

            if User_Input == answer:
                print("Good job! That's correct!")
                break
            else:
                print("Wrong! Loser! You now have",tries ,"left")
                tries += 1

        Give_Up = input("Press 'X' if you'd like to give up here").upper()
        if Give_Up == "X":
            break

    if tries == 3:
        print("You can no longer try. The answer was", answer ,"try again or quit with X")
    
    #0b101
    #1101
def main():
    print("Welcome to the main menu of this programming task.")
    print_command_guide()
    running = True
    
    while running:
        choise = input("What would you like to do?\n\t> ").upper()
        
        match choise:
            case "B":
                to_binary()
                       
            case "D":
                to_decimal()
                
            case "M":
                multiplication_practise()
                
            case "H":
                print_command_guide()
            case "Q":
                running = False
            case _:
                print("No such command, enter 'H' for help.")      
main()


#⠀⠀⠀⠀⠀⠀⠀    ⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#⠀⠀⠀    ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣰⣲⣦⣿⣿⣿⠷⢶⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣪⣿⣿⣿⣿⣿⣿⣿⣶⣽⣷⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#⠀⠀⠀⠀⠀⠀⠀    ⠀⠀⢰⠛⣠⡽⠛⠛⠛⠋⠉⠉⠉⣿⣿⢹⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#⠀⠀⠀    ⠀⠀⠀⠀⠀⠀⠸⢾⣿⠀⠀⣀⡀⠀⣀⡀⠀⢸⣿⠘⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⢠⠇⣾⣿⣀⠬⠿⢹⡄⠹⠿⠯⢸⣏⣾⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠐⠉⠀⣿⡅⣿⠀⢀⣠⣧⠄⣆⠀⢘⣿⡈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠻⣿⣿⠀⢠⣤⣀⣤⣿⢀⣞⠁⠀⠐⠦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣦⠈⢫⣸⠟⠁⢸⡛⠀⠀⠀⠀⠈⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⣦⠘⣁⡀⠀⢸⡿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⠐⣰⢿⣯⡄⠀⠀⣠⡾⠁⣿⣿⣿⣶⣶⣶⣦⣤⣤⣀⣀⠀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⣤⣴⣾⣿⣿⣰⣿⠛⡿⣇⣤⡾⠋⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀⠀⠄⠀⠀⠀
#   ⠀⠀⢠⣶⣾⣿⣿⣿⣿⣿⣿⣿⣟⢀⡷⣮⣭⣤⣤⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⠀⠀⠀⠀⠀
#   ⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⣿⣟⣛⣛⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀
#   ⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠈⣟⣚⣒⣒⣲⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀
#   ⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⡿⠶⠾⠯⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀
#   ⠀⠀⣿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⢿⣭⣿⣿⣟⣿⣿⣿⡏⠀⠉⠉⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⠀⠀
#   ⠀⢠⣿⣿⣿⣿⣿⡿⢻⣿⣿⣿⡿⣻⣛⣛⣛⣛⢻⣿⣿⣿⣇⠀⠀⠀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀
#   ⠀⠻⣿⣿⣿⣿⠅⠂⠰⣿⣿⣿⡇⣿⡿⢿⠯⠭⠭⣽⣿⣿⣿⣤⡀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣄
#   ⠀⣠⣿⣿⡿⠃⠀⠀⠀⢹⣿⣿⡷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
#   ⣰⣿⣿⣿⣣⣷⣄⠀⢠⣾⣿⣿⣇⣿⣿⣓⣒⣒⣺⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇
#   ⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⣿⣿⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣯⣿⣿⣿⣿⣿⣿⣿⣿⡟⠀
#   ⠈⣿⣿⣿⣽⣿⣿⣿⣿⣿⣿⣿⡇⣿⣿⣿⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠉⠉⠉⠉⠉⠀⠀
#   ⠀⠈⠋⠛⠛⠛⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣻⣏⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡀⠀⠀⠀⠀⠀⠀
#   ⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣿⣿⣿⡟⠉⠉⠉⠉⠉⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀⠀




#               .             .
#              / \           / \
#             /   \         / . \
#             | .  \       /  . |
#             | .   |     |  .. |
#             | ..  | _._ |  .. |
#              \..  ./   \.  .. |
#               \. | xxxxx |  ./
#                \/ x ,-. x\__/
#             .--/ ,-'ZZZ`-.  \--.
#             (  ,'ZZ;ZZ;Z;Z`..  )
#             .,'ZZ;; ;; ; ;ZZ `..
#           ._###ZZ @  .  @  Z####`
#            ````Z._  ~~~  _.Z``\
#             _/ ZZ `-----'  Z   \
#            ;   ZZ /.....\  Z    \;;
#           ;/__ ZZ/..  ...\ Z     \;
#          ##'#.\_/.      _.\ZZ     |
#          ##....../      |..\Z     |;
#         / `-.___/|      |../Z     |
#        |    ZZ   |      |./  Z    |;;
#       ;\  Z   /xxxxxxxxxxx\   Z __|
#       ;|   Z    /x\____/x     Z   |;
#        ;\Z  /'##xxxxxxxx###`\__Z .\_
#         Z|/#| ####xxxx####  |##\Z ..|
#      __Z /#/   ####x####    |###\Z_..|
#     /NN\|#|      `###`      \###|NN\..\
#     |NN|\#\  _____.......  _/\/ \__/..|
#     `-'  `-..###########\_/##/  /.../
#            `|#####/   \####|   /../
#              .xxx#|   |xxx.   |./
#             |x' `x|   |'  `|   -
#             `~~~~'    `~~~~'